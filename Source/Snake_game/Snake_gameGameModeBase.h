// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Snake_gameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_GAME_API ASnake_gameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
